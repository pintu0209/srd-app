package com.app.srdonline;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.db.DatabaseHelper;
import com.example.item.ItemListing;
import com.example.util.API;
import com.example.util.BannerAds;
import com.example.util.Constant;
import com.example.util.IsRTL;
import com.example.util.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ListingDetailsActivity extends AppCompatActivity {

    ImageView image, imageMap;
    TextView text, textCategory, txtPhone, txtAddress, txtEmail, txtWebsite, txtTotalView;
    WebView webView;
    ScrollView mScrollView;
    ProgressBar mProgressBar;
    String Id;
    DatabaseHelper databaseHelper;
    ItemListing objBean;
    LinearLayout mAdViewLayout;
    Menu menu;
    boolean isFromNotification = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_details);
        IsRTL.ifSupported(this);
        image = findViewById(R.id.image);
        imageMap = findViewById(R.id.imageMap);
        text = findViewById(R.id.text);
        textCategory = findViewById(R.id.textCategory);
        txtPhone = findViewById(R.id.textPhone);
        txtAddress = findViewById(R.id.textAddress);
        txtEmail = findViewById(R.id.textEmail);
        txtWebsite = findViewById(R.id.textWebsite);
        txtTotalView = findViewById(R.id.textTotalView);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        objBean = new ItemListing();

        Intent i = getIntent();
        Id = i.getStringExtra("Id");
        if (i.hasExtra("isNotification")) {
            isFromNotification = true;
        }
        databaseHelper = new DatabaseHelper(getApplicationContext());

        mAdViewLayout = findViewById(R.id.adView);
        BannerAds.showBannerAds(ListingDetailsActivity.this, mAdViewLayout);

        webView = findViewById(R.id.webView);
        mScrollView = findViewById(R.id.scrollView);
        mProgressBar = findViewById(R.id.progressBar1);
        webView.setBackgroundColor(Color.TRANSPARENT);

        if (NetworkUtils.isConnected(ListingDetailsActivity.this)) {
            getDetails();
        } else {
            showToast(getString(R.string.conne_msg1));
        }

        txtWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWebsite();
            }
        });

        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEmail();
            }
        });

        txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialNumber();
            }
        });

        imageMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListingDetailsActivity.this, MapActivity.class);
                intent.putExtra("latitude", objBean.getListingLatitude());
                intent.putExtra("longitude", objBean.getListingLongitude());
                intent.putExtra("title", objBean.getListingName());
                startActivity(intent);

            }
        });

    }

    private void getDetails() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "get_single_listing");
        jsObj.addProperty("listing_id", Id);
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant.API_URL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                mProgressBar.setVisibility(View.VISIBLE);
                mScrollView.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                mProgressBar.setVisibility(View.GONE);
                mScrollView.setVisibility(View.VISIBLE);

                String result = new String(responseBody);
                try {
                    JSONObject mainJson = new JSONObject(result);
                    JSONArray jsonArray = mainJson.getJSONArray(Constant.ARRAY_NAME);
                    JSONObject objJson;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        objJson = jsonArray.getJSONObject(i);
                        objBean.setId(objJson.getString(Constant.LISTING_ID));
                        objBean.setListingName(objJson.getString(Constant.LISTING_NAME));
                        objBean.setListingImageB(objJson.getString(Constant.LISTING_IMAGE));
                        objBean.setListingPhone(objJson.getString(Constant.LISTING_PHONE));
                        objBean.setListingAddress(objJson.getString(Constant.LISTING_ADDRESS));
                        objBean.setCategoryName(objJson.getString(Constant.CATEGORY_NAME));
                        objBean.setListingLatitude(objJson.getString(Constant.LISTING_LATITUDE));
                        objBean.setListingLongitude(objJson.getString(Constant.LISTING_LONGITUDE));
                        objBean.setListingWebsite(objJson.getString(Constant.LISTING_WEBSITE));
                        objBean.setListingEmail(objJson.getString(Constant.LISTING_EMAIL));
                        objBean.setListingDescription(objJson.getString(Constant.LISTING_DESC));
                        objBean.setTotalView(objJson.getString(Constant.LISTING_TOTAL_VIEW));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setResult();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                mProgressBar.setVisibility(View.GONE);
                mScrollView.setVisibility(View.VISIBLE);
            }

        });
    }

    private void setResult() {


        text.setText(objBean.getListingName());
        textCategory.setText(objBean.getCategoryName());
        txtPhone.setText(objBean.getListingPhone());
        txtAddress.setText(objBean.getListingAddress());
        txtWebsite.setText(objBean.getListingWebsite());
        txtEmail.setText(objBean.getListingEmail());
        txtTotalView.setText(getString(R.string.view, objBean.getTotalView()));

        Picasso.get().load(objBean.getListingImageB()).placeholder(R.drawable.placeholder).into(image);

        String mimeType = "text/html";
        String encoding = "utf-8";
        String htmlText = objBean.getListingDescription();

        String text = "<html><head>"
                + "<style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/custom.ttf\")}body{font-family: MyFont;color: #9E9E9E;text-align:left;font-size:14px;margin-left:0px}"
                + "</style></head>"
                + "<body>"
                + htmlText
                + "</body></html>";

        webView.loadDataWithBaseURL(null, text, mimeType, encoding, null);

    }

    public void showToast(String msg) {
        Toast.makeText(ListingDetailsActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail, menu);
        this.menu = menu;
        isFavourite();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_favourite:
                ContentValues fav = new ContentValues();
                if (databaseHelper.getFavouriteById(Id)) {
                    databaseHelper.removeFavouriteById(Id);
                    menu.getItem(0).setIcon(R.drawable.ic_favourite_1);
                    Toast.makeText(ListingDetailsActivity.this, getString(R.string.favourite_remove), Toast.LENGTH_SHORT).show();
                } else {
                    fav.put(DatabaseHelper.KEY_ID, Id);
                    fav.put(DatabaseHelper.KEY_TITLE, objBean.getListingName());
                    fav.put(DatabaseHelper.KEY_IMAGE, objBean.getListingImageB());
                    fav.put(DatabaseHelper.KEY_CATEGORY, objBean.getCategoryName());
                    fav.put(DatabaseHelper.KEY_PHONE, objBean.getListingPhone());
                    fav.put(DatabaseHelper.KEY_ADDRESS, objBean.getListingAddress());
                    fav.put(DatabaseHelper.KEY_LATITUDE, objBean.getListingLatitude());
                    fav.put(DatabaseHelper.KEY_LONGITUDE, objBean.getListingLongitude());
                    databaseHelper.addFavourite(DatabaseHelper.TABLE_FAVOURITE_NAME, fav, null);
                    menu.getItem(0).setIcon(R.drawable.ic_favourite_hover);
                    Toast.makeText(ListingDetailsActivity.this, getString(R.string.favourite_add), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.menu_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        objBean.getListingName() + "\n" +
                                getString(R.string.share_phone) + " " + objBean.getListingPhone() + "\n" +
                                getString(R.string.share_email) + " " + objBean.getListingEmail() + "\n" +
                                getString(R.string.share_website) + " " + objBean.getListingWebsite() + "\n" +
                                getString(R.string.share_address) + " " + objBean.getListingAddress() + "\n\n" +
                                getString(R.string.share_details_msg) + " " + getPackageName());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }

    private void isFavourite() {
        if (databaseHelper.getFavouriteById(Id)) {
            menu.getItem(0).setIcon(R.drawable.ic_favourite_hover);
        } else {
            menu.getItem(0).setIcon(R.drawable.ic_favourite_1);
        }
    }

    private void openWebsite() {
        startActivity(new Intent(
                Intent.ACTION_VIEW,
                Uri.parse(addHttp(objBean.getListingWebsite()))));
    }

    private void openEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", objBean.getListingEmail(), null));
        emailIntent
                .putExtra(Intent.EXTRA_SUBJECT, "Inquire for the  " + objBean.getListingName());
        startActivity(Intent.createChooser(emailIntent, "Send suggestion..."));
    }

    protected String addHttp(String string1) {
        // TODO Auto-generated method stub
        if (string1.startsWith("http://"))
            return string1;
        else
            return "http://" + string1;
    }

    private void dialNumber() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", objBean.getListingPhone(), null));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (isFromNotification) {
            Intent intent = new Intent(ListingDetailsActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }

    }
}
