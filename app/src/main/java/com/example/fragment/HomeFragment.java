package com.example.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.srdonline.R;
import com.app.srdonline.SearchActivity;
import com.example.adapter.HomeAdapter;
import com.example.item.ItemListing;
import com.example.util.API;
import com.example.util.Constant;
import com.example.util.ItemOffsetDecoration;
import com.example.util.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class HomeFragment extends Fragment {

    NestedScrollView mScrollView;
    ProgressBar mProgressBar;
    ArrayList<ItemListing> mListItem;
    public RecyclerView recyclerView;
    HomeAdapter adapter;
    EditText editText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        mScrollView = rootView.findViewById(R.id.scrollView);
        mProgressBar = rootView.findViewById(R.id.progressBar);
        editText = rootView.findViewById(R.id.etSearch);
        mListItem = new ArrayList<>();
        recyclerView = rootView.findViewById(R.id.rv_featured);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(requireActivity(), R.dimen.item_offset);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setNestedScrollingEnabled(false);

        if (NetworkUtils.isConnected(getActivity())) {
            getHome();
        } else {
            showToast(getString(R.string.conne_msg1));
        }

        editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (!editText.getText().toString().isEmpty()) {
                        Intent intent = new Intent(getActivity(), SearchActivity.class);
                        intent.putExtra("search", editText.getText().toString());
                        startActivity(intent);
                        editText.setText("");
                    }
                    return true;
                }
                return false;
            }
        });

        return rootView;
    }

    private void getHome() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "get_home_listing");
        params.put("data", API.toBase64(jsObj.toString()));
        client.post(Constant.API_URL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                mProgressBar.setVisibility(View.VISIBLE);
                mScrollView.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                mProgressBar.setVisibility(View.GONE);
                mScrollView.setVisibility(View.VISIBLE);

                String result = new String(responseBody);
                try {
                    JSONObject mainJson = new JSONObject(result);
                    JSONArray jsonArray = mainJson.getJSONArray(Constant.ARRAY_NAME);
                    JSONObject objJson;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        objJson = jsonArray.getJSONObject(i);
                        ItemListing objItem = new ItemListing();
                        objItem.setId(objJson.getString(Constant.LISTING_ID));
                        objItem.setListingName(objJson.getString(Constant.LISTING_NAME));
                        objItem.setListingImageB(objJson.getString(Constant.LISTING_IMAGE));
                        objItem.setListingPhone(objJson.getString(Constant.LISTING_PHONE));
                        objItem.setListingAddress(objJson.getString(Constant.LISTING_ADDRESS));
                        objItem.setCategoryName(objJson.getString(Constant.CATEGORY_NAME));
                        objItem.setListingLatitude(objJson.getString(Constant.LISTING_LATITUDE));
                        objItem.setListingLongitude(objJson.getString(Constant.LISTING_LONGITUDE));
                        mListItem.add(objItem);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                displayData();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                mProgressBar.setVisibility(View.GONE);
                mScrollView.setVisibility(View.VISIBLE);
            }

        });
    }

    private void displayData() {
        adapter = new HomeAdapter(getActivity(), mListItem);
        recyclerView.setAdapter(adapter);
    }

    public void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }
}
