package com.example.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.srdonline.MapActivity;
import com.app.srdonline.R;
import com.example.item.ItemListing;
import com.example.util.PopUpAds;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ListingAdapter extends RecyclerView.Adapter<ListingAdapter.ItemRowHolder> {

    private ArrayList<ItemListing> dataList;
    private Context mContext;

    public ListingAdapter(Context context, ArrayList<ItemListing> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    @NonNull
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_listing, parent, false);
        return new ItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder holder, final int position) {
        final ItemListing singleItem = dataList.get(position);
        holder.text.setText(singleItem.getListingName());
        holder.textCategory.setText(singleItem.getCategoryName());
        holder.txtPhone.setText(singleItem.getListingPhone());
        holder.txtAddress.setText(singleItem.getListingAddress());
        Picasso.get().load(singleItem.getListingImageB()).placeholder(R.drawable.placeholder).into(holder.image);
        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopUpAds.showInterstitialAds(mContext, singleItem.getId());
            }
        });
        holder.imageMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MapActivity.class);
                intent.putExtra("latitude", singleItem.getListingLatitude());
                intent.putExtra("longitude", singleItem.getListingLongitude());
                intent.putExtra("title", singleItem.getListingName());
                mContext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    class ItemRowHolder extends RecyclerView.ViewHolder {
        ImageView image, imageMap;
        TextView text, textCategory, txtPhone, txtAddress;
        LinearLayout lyt_parent;

        ItemRowHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            imageMap = itemView.findViewById(R.id.imageMap);
            text = itemView.findViewById(R.id.text);
            textCategory = itemView.findViewById(R.id.textCategory);
            txtPhone = itemView.findViewById(R.id.textPhone);
            txtAddress = itemView.findViewById(R.id.textAddress);
            lyt_parent = itemView.findViewById(R.id.rootLayout);
        }
    }
}
