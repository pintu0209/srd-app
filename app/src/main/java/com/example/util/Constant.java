package com.example.util;

import com.app.srdonline.BuildConfig;

import java.io.Serializable;

public class Constant implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    private static String SERVER_URL = BuildConfig.SERVER_URL;

    public static final String IMAGE_PATH = SERVER_URL + "images/";

    public static final String API_URL = SERVER_URL + "api.php";

    public static final String ARRAY_NAME = "YELLOW_PAGE_APP";

    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_CID = "cid";
    public static final String CATEGORY_IMAGE = "category_image";

    public static final String LISTING_ID = "id";
    public static final String LISTING_NAME = "listing_name";
    public static final String LISTING_DESC = "listing_description";
    public static final String LISTING_ADDRESS = "listing_address";
    public static final String LISTING_EMAIL = "listing_email";
    public static final String LISTING_PHONE = "listing_phone";
    public static final String LISTING_WEBSITE = "listing_website";
    public static final String LISTING_LATITUDE = "listing_latitude";
    public static final String LISTING_LONGITUDE = "listing_longitude";
    public static final String LISTING_IMAGE = "listing_image_b";
    public static final String LISTING_TOTAL_VIEW = "total_views";

    public static final String APP_NAME = "app_name";
    public static final String APP_IMAGE = "app_logo";
    public static final String APP_VERSION = "app_version";
    public static final String APP_AUTHOR = "app_author";
    public static final String APP_CONTACT = "app_contact";
    public static final String APP_EMAIL = "app_email";
    public static final String APP_WEBSITE = "app_website";
    public static final String APP_DESC = "app_description";
    public static final String APP_PRIVACY_POLICY = "app_privacy_policy";

    public static int AD_COUNT = 0;
    public static int AD_COUNT_SHOW;
    public static final String STATUS = "status";

    public static boolean isBanner = false, isInterstitial = false;
    public static String adMobBannerId, adMobInterstitialId, adMobPublisherId;

}
