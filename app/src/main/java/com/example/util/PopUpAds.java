package com.example.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.app.srdonline.ListingDetailsActivity;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.ixidev.gdpr.GDPRChecker;

public class PopUpAds {

    public static void showInterstitialAds(final Context context, final String reviewId) {
        if (Constant.isInterstitial) {
            Constant.AD_COUNT += 1;
            if (Constant.AD_COUNT == Constant.AD_COUNT_SHOW) {
                final InterstitialAd mInterstitial = new InterstitialAd(context);
                mInterstitial.setAdUnitId(Constant.adMobInterstitialId);
                GDPRChecker.Request request = GDPRChecker.getRequest();
                AdRequest.Builder builder = new AdRequest.Builder();
                if (request == GDPRChecker.Request.NON_PERSONALIZED) {
                    Bundle extras = new Bundle();
                    extras.putString("npa", "1");
                    builder.addNetworkExtrasBundle(AdMobAdapter.class, extras);
                }
                mInterstitial.loadAd(builder.build());
                Constant.AD_COUNT = 0;
                mInterstitial.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mInterstitial.show();
                    }

                    @Override
                    public void onAdClosed() {
                        goDetailsScreen(context, reviewId);
                        super.onAdClosed();
                    }

                    @Override
                    public void onAdFailedToLoad(int i) {
                        goDetailsScreen(context, reviewId);
                        super.onAdFailedToLoad(i);
                    }
                });

            } else {
                goDetailsScreen(context, reviewId);
            }
        } else {
            goDetailsScreen(context, reviewId);
        }
    }

    private static void goDetailsScreen(Context context, String reviewId) {
        Intent intent = new Intent(context, ListingDetailsActivity.class);
        intent.putExtra("Id", reviewId);
        context.startActivity(intent);
    }
}
